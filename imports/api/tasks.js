/**
 * Created by f.putra on 9/21/16.
 */
import { Mongo } from 'meteor/mongo';

export const Tasks = new Mongo.Collection('tasks');